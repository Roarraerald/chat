var socket = io();

socket.on("connect", function () {
  console.log("Conectado al servidor");
});

socket.on("disconnect", function () {
  console.log("Perdimos conexión con el servidor");
});

//Enviar información
socket.emit(
  "enviarMensaje",
  {
    usuario: "Ronald",
    mensaje: "Hola Mundo",
  }
);

//Escuchar información
socket.on("enviarMensaje", function (mensaje) {
  console.log("Servidor mensaje:", mensaje);
});
